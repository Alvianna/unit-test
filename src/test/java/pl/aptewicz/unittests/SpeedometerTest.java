package pl.aptewicz.unittests;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import pl.aptewicz.unittests.speedometer.Speedometer;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SpeedometerTest {
    //private static Speedometer speedometer;

    @Test
    void shouldCalculateSpeedWhenUserProvidesGoodNumbersAsTimeAndDistance(){
        // given
       Speedometer data = new Speedometer(60, 30);

        //when
        final double speed = data.calculateSpeed();

        //then
        final double exSpeed = 120.00;
        Assertions.assertThat(speed).isFinite().isPositive().isEqualTo(exSpeed);
    }
}